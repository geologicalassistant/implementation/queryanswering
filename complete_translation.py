from translator import *
from dictionaries import *

def printWholeConfig():
    categories = ['Source Rock','Hydrocarbon','Migration Pathway',
                'SubmarineFan of GeoUnit5','Permeability of GeoUnit5','Porosity of GeoUnit5',
                'SubmarineFan of GeoUnit8','Permeability of GeoUnit8','Porosity of GeoUnit8',
                'SubmarineFan of GeoUnit11','Permeability of GeoUnit11','Porosity of GeoUnit11',
                'SubmarineFan of GeoUnit14','Permeability of GeoUnit14','Porosity of GeoUnit14',
                'Fault0','Fault1','Fault2','Fault3',
                'MigrationTime vs. Fault3','MigrationTime vs. Fault0','MigrationTime vs. Fault1','MigrationTime vs. Fault2',
                'Trap for GeoUnit5','Trap for GeoUnit8','Trap for GeoUnit11','Trap for GeoUnit14',
                'Accumulation in GeoUnit5','Accumulation in GeoUnit8','Accumulation in GeoUnit11','Accumulation in GeoUnit14']

    fileNames = []
    content = []
    #previous = []
    counter = 0

    files = open('allFileNames.txt','r')

    for file in files:
        hc_l = []
        mp_l = []
        geo_l = []
        fault_l = []
        ft_l = []
        mt_l = []
        trapf_l = []
        ac_l = []
        ac_quantity_l = []
        trap_history_l = []
        history_l = []

        hc_excel_l = []
        mp_excel_l = []
        geo_excel_l = []
        fault_excel_l = []
        ft_excel_l = []
        mt_excel_l = []
        trapf_excel_l = []
        ac_excel_l = []
        complete_report = []
        complete_report_excel = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("< Pathway.*>", ln):
                mp = interpreteMigrationPathway(ln)
                if mp == "":
                    mp = "There is no migration pathway."
                mp_l.append(mp)
                mp_excel_l.append(mp)

            elif re.search(r'younger\(.*Migration.*', ln):
                mt, mt_excel = interpreteMigrationTime(ln)
                mt_l.append(mt)
                mt_excel_l.append(mt_excel)

            elif re.search(r'younger\(.*Fault.*Fault.*\)', ln):
                ft, ft_excel = interpreteFaultTime(ln)
                ft_l.append(ft)
                ft_excel_l.append(ft_excel)

            elif re.search(r'sameAge\(.*Fault.*Fault.*\)', ln):
                ft, ft_excel = interpreteFaultTime(ln)
                ft_l.append(ft)
                ft_excel_l.append(ft_excel)

            elif re.search("accumulation\(.*\)", ln):
                ac, ac_excel = interpreteAccumulation(ln)
                ac_l.append(ac)
                ac_excel_l.append(ac_excel)

            elif re.findall("<.*Hydrocarbon.*HType:.*>", ln):
                hc_type, hc_type_excel = interpreteHydrocarbon(ln)
                hc_l.append(hc_type)
                hc_excel_l.append(hc_type_excel)

            elif re.findall("trapformation\(.*\)", ln):
                trapf, trapf_excel = interpreteTrapformation(ln)
                trapf_l.append(trapf)
                trapf_excel_l.append(trapf_excel)
                if not re.search("cannot be trapped.", trapf):
                    geoId = re.findall(r'\b\d+\b', ln)[0]
                    ac_quantity = accumulation_quantity[ln.split(',')[2].strip( )]
                    ac_quantity_l.append("The accumulation in GeoUnit " + geoId + " can be " + ac_quantity + ".")

            elif re.findall("<.*GeoUnit.*sandstone.*>", ln):
                submarineFan, permeability_porosity, submarineFan_excel, permeability_excel, porosity_excel = interpreteGeoUnit(ln)
                geo_l.append(submarineFan)
                geo_l.append(permeability_porosity)
                geo_excel_l.append(submarineFan_excel)
                geo_excel_l.append(permeability_excel)
                geo_excel_l.append(porosity_excel)

            elif re.findall("<.*Fault.*InContactWith:.*>", ln):
                fault, fault_excel = interpreteFault(ln)
                fault_l.append(fault)
                fault_excel_l.append(fault_excel)

            elif re.findall(r'history\(.*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip( ).strip('history(').strip('::').strip( ).strip('"')
                trap_history_l.append(hst)
            elif re.findall('\".*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip('"')
                trap_history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip(')').strip('"')
                trap_history_l.append(hst)

            elif re.findall(r'history\(.*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip( ).strip('history(').strip('::').strip( ).strip('"')
                history_l.append(hst)
            elif re.findall('\".*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip('"')
                history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip(')').strip('"')
                history_l.append(hst)


        complete_report += ['GeoUnit 4 is the source rock.']
        complete_report_excel = ['GeoUnit4']
        complete_report += ['--HYDROCARBON--']
        complete_report += hc_l
        complete_report_excel += hc_excel_l
        complete_report += ['--MIGRATION PATHWAY--']
        complete_report += mp_l
        complete_report_excel += mp_excel_l
        complete_report += ['--SUBMARINE FAN--']
        complete_report += geo_l
        complete_report_excel += geo_excel_l
        complete_report += ['--FAULT TYPE--']
        complete_report += fault_l
        complete_report_excel += fault_excel_l
        #complete_report += ['--FAULTING TIME--']
        #complete_report += ft_l
        #complete_report_excel += ft_excel_l
        complete_report += ['--MIGRATION TIME--']
        complete_report += mt_l
        complete_report_excel += mt_excel_l
        complete_report += ['--TRAP--']
        for x in trapf_l:
            complete_report += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            for t in trap_history_l:
                if geoId == re.findall(r'\b\d+\b', t)[0]:
                    complete_report += ["<Reason>: " + t]
        complete_report_excel += trapf_excel_l
        complete_report += ['--ACCUMULATION--']
        for x in ac_l:
            complete_report += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            if re.search('GeoUnit ' + geoId + ' has accumulation.',x):
                for acq in ac_quantity_l:
                    if geoId == re.findall(r'\b\d+\b', acq)[0]:
                        complete_report += ["  " + acq]
                        break
        complete_report_excel += ac_excel_l
        complete_report += ['--HISTORY OF HYDROCARBON MIGRATION AND ACCUMULATION--']
        complete_report += history_l

        print('\n***************************************** '+ file.strip('\n') +' ****************************************')
        for ln in complete_report:
            print(ln)

        counter += 1

        #if not isIncluded(previous, complete_report):
        #    counter += 1
        #    print("***************************************************************")
        #    for ln in complete_report:
        #        print(ln)
        #    print("***************************************************************", end='\n\n')
        #    previous.append(complete_report)

        fileNames.append(file.strip('\n'))
        #content.append(complete_report)
        content.append(complete_report_excel)
        readFile.close()

    files.close()

    #createExcel(fileNames, categories, content)

    if counter == 1:
        print('\n' + str(counter) + ' scenario.')
    else:
        print('\n' + str(counter) + ' scenarios.')
