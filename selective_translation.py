from functions import *
from translator import *
from dictionaries import *


def printUpToGeoUnit(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        hc_l = []
        mp_l = []
        geo_l = []
        fault_l = []
        ft_l = []
        mt_l = []
        trapf_l = []
        ac_l = []
        ac_quantity_l = []
        trap_history_l = []
        history_l = []
        complete_report = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("< Pathway.*>", ln):
                mp = interpreteMigrationPathway(ln)
                if mp == "":
                    mp = "There is no migration pathway from the source rock to GeoUnit " + geoId + ". Because hydrocarbon was not spilled from the source rock."
                elif int(mp.split(' ')[-1]) in geos_before_geo[int(geoId)]:
                    mp = "There is no migration pathway from the source rock to GeoUnit " + geoId + ". Because hydrocarbon was not spilled from GeoUnit " + mp.split(' ')[-1] + ". ("+ mp + ")"
                elif not int(mp.split(' ')[-1]) in geos_before_and_include_geo[int(geoId)]:
                    mp = cutThePathwayOnlyUntilSpecificGeounit(geoId,mp)
                mp_l.append(mp)

            elif re.search(r'younger\(.*Migration.*', ln) and int(re.findall(r'\b\d+\b', ln)[0]) in faults_before_and_include_geo[int(geoId)]:
                mt, mt_excel = interpreteMigrationTime(ln)
                mt_l.append(mt)

            elif re.search(r'younger\(.*Fault.*Fault.*\)', ln) and int(re.findall(r'\b\d+\b', ln)[0]) in faults_before_and_include_geo[int(geoId)]:
                ft, ft_excel = interpreteFaultTime(ln)
                ft_l.append(ft)

            elif re.search(r'sameAge\(.*Fault.*Fault.*\)', ln) and int(re.findall(r'\b\d+\b', ln)[0]) in faults_before_and_include_geo[int(geoId)]:
                ft, ft_excel = interpreteFaultTime(ln)
                ft_l.append(ft)

            elif re.search("accumulation\(.*\)", ln) and int(re.findall(r'\b\d+\b', ln)[0]) in geos_before_and_include_geo[int(geoId)]:
                ac, ac_excel = interpreteAccumulation(ln)
                ac_l.append(ac)

            elif re.findall("<.*Hydrocarbon.*HType:.*>", ln):
                hc_type, hc_type_excel = interpreteHydrocarbon(ln)
                hc_l.append(hc_type)

            elif re.findall("trapformation\(.*\)", ln) and int(re.findall(r'\b\d+\b', ln)[0]) in geos_before_and_include_geo[int(geoId)]:
                trapf, trapf_excel = interpreteTrapformation(ln)
                trapf_l.append(trapf)

                if not re.search("cannot be trapped.", trapf):
                    id = re.findall(r'\b\d+\b', ln)[0]
                    ac_quantity = accumulation_quantity[ln.split(',')[2].strip( )]
                    ac_quantity_l.append("The accumulation in GeoUnit " + id + " can be " + ac_quantity + ".")

            elif re.findall("<.*GeoUnit.*sandstone.*>", ln) and int(re.findall(r'\b\d+\b', ln)[0]) in geos_before_and_include_geo[int(geoId)]:
                submarineFan, permeability_porosity, submarineFan_excel, permeability_excel, porosity_excel = interpreteGeoUnit(ln)
                geo_l.append(submarineFan)
                geo_l.append(permeability_porosity)

            elif re.findall("<.*Fault.*InContactWith:.*>", ln) and int(re.findall(r'\b\d+\b', ln)[0]) in faults_before_and_include_geo[int(geoId)]:
                fault, fault_excel = interpreteFault(ln)
                fault_l.append(fault)

            elif re.findall(r'history\(.*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip( ).strip('history(').strip('::').strip( ).strip('"')
                trap_history_l.append(hst)
            elif re.findall('\".*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip('"')
                trap_history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip(')').strip('"')
                trap_history_l.append(hst)

            elif re.findall(r'history\(.*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip( ).strip('history(').strip('::').strip( ).strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)
            elif re.findall('\".*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip('::').strip( ).strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip('::').strip( ).strip(')').strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)


        complete_report += ['GeoUnit 4 is the source rock.']
        complete_report += ['--HYDROCARBON--']
        complete_report += hc_l
        complete_report += ['--MIGRATION PATHWAY--']
        complete_report += mp_l
        complete_report += ['--SUBMARINE FAN--']
        complete_report += geo_l
        complete_report += ['--FAULT TYPE--']
        complete_report += fault_l
        #complete_report += ['--FAULTING TIME--']
        #complete_report += ft_l
        complete_report += ['--MIGRATION TIME--']
        complete_report += mt_l
        complete_report += ['--TRAP--']
        for x in trapf_l:
            complete_report += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            for t in trap_history_l:
                if geoId == re.findall(r'\b\d+\b', t)[0]:
                    complete_report += ["<Reason>: " + t]
        complete_report += ['--ACCUMULATION--']
        for x in ac_l:
            complete_report += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            if re.search('GeoUnit ' + geoId + ' has accumulation.', x):
                for acq in ac_quantity_l:
                    if geoId == re.findall(r'\b\d+\b', acq)[0]:
                        complete_report += ["  " + acq]
                        break
        complete_report += ['--HISTORY OF HYDROCARBON MIGRATION AND ACCUMULATION--']
        complete_report += history_l

        if not isIncluded(previous, complete_report):
            counter += 1
            print("**********************************************************************************************************************************************************")
            for ln in complete_report:
                print(ln)
            print("**********************************************************************************************************************************************************", end='\n\n')
            previous.append(complete_report)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' scenario.')
    else:
        print('\n' + str(counter) + ' scenarios.')


def printHydrocarbon():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        hc_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*Hydrocarbon.*HType:.*>", ln):
                hc_type, hc_type_excel = interpreteHydrocarbon(ln)
                hc_l.append(hc_type)

        if not isIncluded(previous, hc_l):
            counter += 1
            print("\n***************************************")
            for ln in hc_l:
                print(ln)
            print("***************************************", end='\n\n')
            previous.append(hc_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllFaults():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        fault_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*Fault.*InContactWith:.*>", ln):
                fault, fault_excel = interpreteFault(ln)
                fault_l.append(fault)

        if not isIncluded(previous, fault_l):
            counter += 1
            print("\n***************************")
            for ln in fault_l:
                print(ln)
            print("***************************", end='\n\n')
            previous.append(fault_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printUptoFault(faultId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        fault_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*Fault.*InContactWith:.*>", ln):
                getId = re.findall(r'\b\d+\b', ln)[0]
                if int(getId) in faults_before_and_include_fault[int(faultId)]:
                    fault, fault_excel = interpreteFault(ln)
                    fault_l.append(fault)

        if not isIncluded(previous, fault_l):
            counter += 1
            print("\n******************************")
            for ln in fault_l:
                print(ln)
            print("******************************", end='\n\n')
            previous.append(fault_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificFault(faultId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        fault_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*" + faultId + ".*Fault.*InContactWith:.*>", ln):
                fault, fault_excel = interpreteFault(ln)
                fault_l.append(fault)

        if not isIncluded(previous, fault_l):
            counter += 1
            print("\n******************************")
            for ln in fault_l:
                print(ln)
            print("******************************", end='\n\n')
            previous.append(fault_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllGeoUnits():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        geo_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*GeoUnit.*sandstone.*>", ln):
                submarineFan, permeability_porosity, submarineFan_excel, permeability_excel, porosity_excel = interpreteGeoUnit(ln)
                geo_l.append(submarineFan)
                geo_l.append(permeability_porosity)

        if not isIncluded(previous, geo_l):
            counter += 1
            print("\n**********************************************")
            for ln in geo_l:
                print(ln)
            print("***********************************************", end='\n\n')
            previous.append(geo_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')

def printUptoGeoUnit(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        geo_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*GeoUnit.*sandstone.*>", ln):
                getId = re.findall(r'\b\d+\b', ln)[0]
                if int(getId) in geos_before_and_include_geo[int(geoId)]:
                    submarineFan, permeability_porosity, submarineFan_excel, permeability_excel, porosity_excel = interpreteGeoUnit(ln)
                    geo_l.append(submarineFan)
                    geo_l.append(permeability_porosity)

        if not isIncluded(previous, geo_l):
            counter += 1
            print("\n*************************************************")
            for ln in geo_l:
                print(ln)
            print("**************************************************", end='\n\n')
            previous.append(geo_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificGeoUnit(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        geo_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("<.*" + geoId + ".*GeoUnit.*sandstone.*>", ln):
                submarineFan, permeability_porosity, submarineFan_excel, permeability_excel, porosity_excel = interpreteGeoUnit(ln)
                geo_l.append(submarineFan)
                geo_l.append(permeability_porosity)

        if not isIncluded(previous, geo_l):
            counter += 1
            print("\n*************************************************")
            for ln in geo_l:
                print(ln)
            print("**************************************************", end='\n\n')
            previous.append(geo_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllTraps():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        trapf_l = []
        trap_history_l = set()
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("trapformation\(.*\)", ln):
                trapf, trapf_excel = interpreteTrapformation(ln)
                trapf_l.append(trapf)
            elif re.findall(r'history\(.*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip().strip('history(').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*::', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*\"\)', ln) and re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip().strip(')').strip('"')
                trap_history_l.add(hst)

        for x in trapf_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            # ------------ print out the detailed information about the traps (comment it out if not needed)------------
            for t in trap_history_l:
                if geoId == re.findall(r'\b\d+\b', t)[0]:
                    merge += ["<Reason>: " + t]
            #-----------------------------------------------------------------------------------------------------------


        if not isIncluded(previous, merge):
            counter += 1
            print("\n****************************************************************************************************************************************************************************")
            for ln in merge:
                print(ln)
            print("****************************************************************************************************************************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printUptoTrap(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        trapf_l = []
        trap_history_l = set()
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("trapformation\(.*\)", ln):
                getId = re.findall(r'\b\d+\b', ln)[0]
                if int(getId) in geos_before_and_include_geo[int(geoId)]:
                    trapf, trapf_excel = interpreteTrapformation(ln)
                    trapf_l.append(trapf)
            elif re.findall(r'history\(.*::', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip().strip('history(').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*::', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*\"\)', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip('::').strip().strip(')').strip('"')
                trap_history_l.add(hst)

        for x in trapf_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            # ------------ print out the detailed information about the trap (comment it out if not needed)------------
            for t in trap_history_l:
                if geoId == re.findall(r'\b\d+\b', t)[0]:
                    merge += ["<Reason>: " + t]
            # -----------------------------------------------------------------------------------------------------------

        if not isIncluded(previous, merge):
            counter += 1
            print("\n************************************************************************************************************************************************************************************************************")
            for ln in merge:
                print(ln)
            print("************************************************************************************************************************************************************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificTrap(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        trapf_l = []
        trap_history_l = set()
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall("trapformation\(" + geoId +".*\)", ln):
                trapf, trapf_excel = interpreteTrapformation(ln)
                trapf_l.append(trapf)
            elif re.findall(r'history\(.*::', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip().strip('history(').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*::', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip('::').strip().strip('"')
                trap_history_l.add(hst)
            elif re.findall('\".*\"\)', ln) and re.findall(r'.*trap-formation.*', ln):# and re.findall('GeoUnit ' + geoId, ln):
                hst = ln.strip('\n').strip('::').strip().strip(')').strip('"')
                trap_history_l.add(hst)

        for x in trapf_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            # ------------ print out the detailed information about the trap (comment it out if not needed)------------
            for t in trap_history_l:
                if geoId == re.findall(r'\b\d+\b', t)[0]:
                    merge += ["<Reason>: " + t]
            # -----------------------------------------------------------------------------------------------------------

        if not isIncluded(previous, merge):
            counter += 1
            print("\n************************************************************************************************************************************************************************************************************")
            for ln in merge:
                print(ln)
            print("************************************************************************************************************************************************************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllAccumulations():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        ac_l = []
        ac_quantity_l = []
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("accumulation\(.*\)", ln):
                ac, ac_excel = interpreteAccumulation(ln)
                ac_l.append(ac)
            elif re.search("trapformation\(.*\)", ln):
                trapf, trapf_excel = interpreteTrapformation(ln)
                if not re.search("cannot be trapped.", trapf):
                    geoId = re.findall(r'\b\d+\b', ln)[0]
                    ac_quantity = accumulation_quantity[ln.split(',')[2].strip( )]
                    ac_quantity_l.append("The accumulation in GeoUnit " + geoId + " can be " + ac_quantity + ".")

        for x in ac_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            if re.search('GeoUnit ' + geoId + ' has accumulation.', x):
                for acq in ac_quantity_l:
                    if re.search("GeoUnit " + geoId, acq):
                        merge += ["  " + acq]
                        break

        if not isIncluded(previous, merge):
            counter += 1
            print("*********************************************************************")
            for ln in merge:
                print(ln)
            print("*********************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')

def printUptoAccumulation(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        ac_l = []
        ac_quantity_l = []
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("accumulation\(.*\)", ln):
                getId = re.findall(r'\b\d+\b', ln)[0]
                if int(getId) in geos_before_and_include_geo[int(geoId)]:
                    ac, ac_excel = interpreteAccumulation(ln)
                    ac_l.append(ac)
            elif re.search("trapformation\(.*\)", ln):
                getId = re.findall(r'\b\d+\b', ln)[0]
                if int(getId) in geos_before_and_include_geo[int(geoId)]:
                    trapf, trapf_excel = interpreteTrapformation(ln)
                    if not re.search(".*cannot be trapped.", trapf):
                        myId = re.findall(r'\b\d+\b', ln)[0]
                        ac_quantity = accumulation_quantity[ln.split(',')[2].strip( )]
                        ac_quantity_l.append("The accumulation in GeoUnit " + myId + " can be " + ac_quantity + ".")

        for x in ac_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            if re.search('GeoUnit ' + geoId + ' has accumulation.', x):
                for acq in ac_quantity_l:
                    if re.search("GeoUnit " + geoId, acq):
                        merge += ["  " + acq]
                        break

        if not isIncluded(previous, merge):
            counter += 1
            print("*********************************************************************")
            for ln in merge:
                print(ln)
            print("*********************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificAccumulation(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        ac_l = []
        ac_quantity_l = []
        merge = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("accumulation\(" + geoId + ".*\)", ln):
                ac, ac_excel = interpreteAccumulation(ln)
                ac_l.append(ac)
            elif re.search("trapformation\(" + geoId +".*\)", ln):
                trapf, trapf_excel = interpreteTrapformation(ln)
                if not trapf == "GeoUnit " + geoId + " cannot be trapped.":
                    ac_quantity = accumulation_quantity[ln.split(',')[2].strip( )]
                    ac_quantity_l.append("The accumulation in GeoUnit " + geoId + " can be " + ac_quantity + ".")

        for x in ac_l:
            merge += [x]
            geoId = re.findall(r'\b\d+\b', x)[0]
            if re.search('GeoUnit ' + geoId + ' has accumulation.', x):
                for acq in ac_quantity_l:
                    if re.search("GeoUnit " + geoId, acq):
                        merge += ["  " + acq]
                        break

        if not isIncluded(previous, merge):
            counter += 1
            print("*********************************************************************")
            for ln in merge:
                print(ln)
            print("*********************************************************************", end='\n\n')
            previous.append(merge)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllPathways():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        mp_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("< Pathway.*>", ln):
                mp = interpreteMigrationPathway(ln)
                if mp == "":
                    mp = "There is no migration pathway."
                mp_l.append(mp)

        if not isIncluded(previous, mp_l):
            counter += 1
            print("********************************************************************************************************")
            for ln in mp_l:
                print(ln)
            print("********************************************************************************************************", end='\n\n')
            previous.append(mp_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllPathwaysNotBeyondGeoUnit(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        mp_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("< Pathway.*>", ln):
                mp = interpreteMigrationPathway(ln)
                if mp == "":
                    mp = "There is no migration pathway from the source rock to GeoUnit " + geoId + ". Because hydrocarbon was not spilled from the source rock."
                elif int(mp.split(' ')[-1]) in geos_before_geo[int(geoId)]:
                    mp = "There is no migration pathway from the source rock to GeoUnit " + geoId + ". Because hydrocarbon was not spilled from GeoUnit " + mp.split(' ')[-1] + ". ("+ mp + ")"
                elif not int(mp.split(' ')[-1]) in geos_before_and_include_geo[int(geoId)]:
                    mp = cutThePathwayOnlyUntilSpecificGeounit(geoId,mp)
                mp_l.append(mp)

        if not isIncluded(previous, mp_l):
            counter += 1
            print("****************************************************************************************************************************************************")
            for ln in mp_l:
                print(ln)
            print("****************************************************************************************************************************************************", end='\n\n')
            previous.append(mp_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificPathways(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        mp_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search("< Pathway.*>", ln):
                mp = interpreteMigrationPathway(ln)
                if re.search("GeoUnit " + geoId, mp):
                    last_geoUnit_in_mp = mp.split(' ')[-1]
                    if last_geoUnit_in_mp == geoId:
                        mp_l.append(mp)
                    elif not last_geoUnit_in_mp in geos_before_and_include_geo[int(geoId)]:
                        mp = cutThePathwayOnlyUntilSpecificGeounit(geoId, mp)
                        mp_l.append(mp)

        if not mp_l == [] and not isIncluded(previous, mp_l):
            counter += 1
            print("********************************************************************************************************")
            for ln in mp_l:
                print(ln)
            print("********************************************************************************************************", end='\n\n')
            previous.append(mp_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllMigrationTimes():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        mt_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.search(r'younger\(.*Migration.*', ln):
                mt, mt_excel = interpreteMigrationTime(ln)
                mt_l.append(mt)

        if not isIncluded(previous, mt_l):
            counter += 1
            print("**************************************************")
            for ln in mt_l:
                print(ln)
            print("**************************************************", end='\n\n')
            previous.append(mt_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printAllMigrationAccumulationHistory():
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        history_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall(r'history\(.*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip( ).strip('history(').strip('::').strip( ).strip('"')
                history_l.append(hst)
            elif re.findall('\".*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip('"')
                history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and not re.findall(r'.*trap-formation.*', ln):
                hst = ln.strip('\n').strip('::').strip( ).strip(')').strip('"')
                history_l.append(hst)

        if not isIncluded(previous, history_l):
            counter += 1
            print("**********************************************************************************************************************************************************************************************")
            for ln in history_l:
                print(ln)
            print("**********************************************************************************************************************************************************************************************", end='\n\n')
            previous.append(history_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')


def printSpecificMigrationAccumulationHistory(geoId):
    counter = 0
    previous = []
    files = open('allFileNames.txt','r')

    for file in files:
        history_l = []

        readFile = open(file.strip('\n'), 'r')
        for ln in readFile:
            if re.findall(r'history\(.*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip().strip('history(').strip('::').strip( ).strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)
            elif re.findall('\".*::', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip('::').strip().strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)
            elif re.findall('\".*\"\)', ln) and not re.findall(r'.*trap-formation.*', ln):
                is_found = False
                hst = ln.strip('\n').strip('::').strip().strip(')').strip('"')
                for x in geos_beyond_geo[int(geoId)]:
                    if re.findall('GeoUnit ' + str(x), hst):
                        is_found = True
                        break
                if not is_found:
                    history_l.append(hst)

        if not isIncluded(previous, history_l):
            counter += 1
            print("**********************************************************************************************************************************************************************************************")
            for ln in history_l:
                print(ln)
            print("**********************************************************************************************************************************************************************************************", end='\n\n')
            previous.append(history_l)

        readFile.close()

    files.close()

    if counter == 1:
        print('\n' + str(counter) + ' variation.')
    else:
        print('\n' + str(counter) + ' variations.')