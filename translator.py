import re


def interpreteMigrationPathway(ln):
    plst = ln.split(',Ocnt')[0].split('Pathways:')[1]
    extraction = [x.replace('(', ' ').replace(')', ' ').replace(',', ' ').strip(' ') for x in plst.split('::')]
    extraction.remove('nil')
    isTheFirstSubPath = True
    subpath_concatenation = []
    migrationPathway = ""

    if not extraction == []:
        for idx in range(len(extraction)-1,-1,-1):
            elements = extraction[idx].split(' ')
            if len(elements) == 2:
                if isTheFirstSubPath:
                    subpath_concatenation.append("GeoUnit " + elements[0] + " -> " + "GeoUnit " + elements[1])
                    isTheFirstSubPath = False
                else:
                    subpath_concatenation.append("GeoUnit " + elements[1])
            elif len(elements) == 3:
                if isTheFirstSubPath:
                    subpath_concatenation.append("GeoUnit " + elements[1] + " -> Fault " + elements[0] + " -> " + "GeoUnit " + elements[2])
                    isTheFirstSubPath = False
                else:
                    subpath_concatenation.append("Fault " + elements[0] + " -> " + "GeoUnit " + elements[2])
        if len(subpath_concatenation) > 1:
            for idx in range(len(subpath_concatenation)-1):
                migrationPathway += subpath_concatenation[idx] + " -> "
        migrationPathway += subpath_concatenation[-1]
    return migrationPathway


def interpreteMigrationTime(ln):
    if re.search("younger\(timeOf\(Fault.*Migration\)\)", ln):
        faultId = re.findall(r'\b\d+\b', ln)
        return ("Migration happened before Fault " + faultId[0] + " was ceased.", "before")
    elif re.search("younger\(timeOf\(Migration.*Fault.*\)", ln):
        faultId = re.findall(r'\b\d+\b', ln)
        return ("Migration happened after Fault " + faultId[0] + " was ceased.", "after")


def interpreteFaultTime(ln):
    if re.search(r'younger\(.*Fault.*Fault.*', ln):
        faultIds = re.findall(r'\b\d+\b', ln)
        return ("Fault " + faultIds[0] + " was formed after Fault " + faultIds[1] + ".", "earlier")
    elif re.search(r'sameAge\(.*Fault.*Fault.*', ln):
        faultIds = re.findall(r'\b\d+\b', ln)
        return ("Fault " + faultIds[0] + " was formed during the same period as Fault " + faultIds[1] + ".", "the same")


def interpreteAccumulation(ln):
    geoId = re.findall(r'\b\d+\b', ln)
    if re.search("accumulation\(.*true\)", ln):
        return ("GeoUnit " + geoId[0] + " has accumulation.", "Yes")
    elif re.search("accumulation\(.*false\)", ln):
        return ("GeoUnit " + geoId[0] + " does not have accumulation.", "No")


def interpreteHydrocarbon(ln):
    hc_type = ln.strip(' ').split(' ')[6].split('"')[1]
    return ("The type of hydrocarbon is " + hc_type + ".", hc_type)


def interpreteTrapformation(ln):
    ln = ln.strip(' ')
    if re.search("trapformation\(.*incomparable\)", ln):
        id = re.findall(r'\b\d+\b', ln)
        return ("GeoUnit " + id[0] + " cannot be trapped.", "No")
    else:
        ids = re.findall(r'\b\d+\b', ln)
        return ("GeoUnit " + ids[0] + " can be trapped and the trapped was formed when Fault " + ids[-1] + " was ceased.", "Yes")


def interpreteGeoUnit(ln):
    ln = ln.strip(' ')
    geoId = re.findall(r'\b\d+\b', ln)[0]
    submarineFan = ln.split('SubmarineFan: ')[1].split(',')[0]
    permeability = ln.split('Permeability: ')[1].split(',')[0]
    porosity = ln.split('Porosity: ')[1].split(',')[0]
    return ("GeoUnit " + geoId + " was deposited in " + submarineFan + "." , "GeoUnit " + geoId + " is " + permeability + " and " +  porosity + ".", submarineFan, permeability, porosity)


def interpreteFault(ln):
    ln = ln.strip(' ')
    faultId = re.findall(r'\b\d+\b', ln)[0]
    filling = ln.split('Filling: ')[1].split(',')[0]
    return ("Fault " + faultId + " is " + filling + ".", filling)
