#import pandas as pd


#def createExcel(rows, categories, content):
#	df = pd.DataFrame(content, index=rows, columns=categories)
#	df.to_excel("overview.xlsx")


def isIncluded(list_of_lists, lst):
	for el in list_of_lists:
		if el == lst:
			return True
	return False


def hasPathway(id, mp):
	if 'GeoUnit '+ str(id) in mp:
		return True
	else:
		return False

def cutThePathwayOnlyUntilSpecificGeounit(geoId,mp):
	new_mp = ""
	removeArrow = mp.split('->')
	for x in removeArrow:
		if not x.strip(' ') == "GeoUnit " + geoId:
			new_mp += (x.strip(' ') + " -> ")
		elif x.strip(' ') == "GeoUnit " + geoId:
			new_mp += x.strip(' ')
			break
	return new_mp
