# user selections :

filtering_category = {1:"fault sealing capacity", 2:"reservoir type", 3:"trap-formation", 4:"migration-time", 5:"pathway until a possible reservoir", 6:"accumulation"}

submarineFan_category = {1:"feederChannel", 2:"distributaryChannel", 3:"interChannel", 4:"lobe", 5:"lobeFringe", 6:"basinPlain"}

filling_category = {1:"sealing", 2:"non-sealing"}

accumulation_category = {1:"true", 2:"false"}

age_comparison = {1:"younger", 2:"older"}

print_result_category = {1:"the type of hydrocarbon", 2:"all-fault", 3:"upto-fault", 4:"specific-fault", 5:"all-geounit", 6:"upto-geounit", 7:"specific-geounit",
                         8:"all-trapformation", 9:"upto-trapformation", 10:"specific-trapformation", 11:"all-accumulation", 12:"upto-accumulation",
                         13:"specific-accumulation", 14:"all-pathway", 15:"all pathways not beyond a GeoUnit", 16:"pathways of a specific GeoUnit", 17:"migration-time",
                         18:"complete geological processes of hydrocarbon migration and accumulation",
                         19:"geological processes of hydrocarbon migration and accumulation not beyond a GeoUnit",
                         20:"all the scenarios", 21:"scenarios up to a specific GeoUnit"}


# relations between sandstones, faults, and traps :

sandstone_ordering = (5,8,11,14)

faults_before_geo = {5:(), 8:(0,), 11:(0,1), 14:(0,1,2)}

faults_before_and_include_fault = {0:(0,), 1:(0,1), 2:(0,1,2), 3:(0,1,2,3)}

faults_before_and_include_geo = {5:(0,), 8:(0,1), 11:(0,1,2), 14:(0,1,2,3)}

geos_before_and_include_geo = {5:(5,), 8:(5,8), 11:(5,8,11), 14:(5,8,11,14)}

geos_before_geo = {5:(), 8:(5,), 11:(5,8), 14:(5,8,11)}

geos_beyond_geo = {5:(8,11,14), 8:(11,14), 11:(14,), 14:()}

trap_formed_by_fault = {0:5, 1:8, 2:11, 3:14}

geo_to_fault = {5:0, 8:1, 11:2, 14:3}

fault_to_geo = {0:5, 1:8, 2:11, 3:14}


# text improvement

accumulation_quantity = {"fillToMaxClosure":"filled to maximum closure", "fillToJuxtaposition":"filled to the juxtaposition"}

