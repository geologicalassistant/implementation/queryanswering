from selective_translation import *
from complete_translation import *

#--------------- initialise the complete file names in allFileNames.txt -----------------
all_files = [] # for the support of back button
open('allFileNames.txt', 'w').close()
for x in range(1, 4):
    for y in range(1, 1009):
        writeFile = open('allFileNames.txt','a')
        writeFile.write('example-output/scenario-' + str(x) + '-' + str(y) + '-out.txt\n')
        all_files.append('example-output/scenario-' + str(x) + '-' + str(y) + '-out.txt\n')
        writeFile.close()
#----------------------------------------------------------------------------------------


# global variables initialization
applied_filters = []
fileNames_history = []
fileNames_history.append(all_files)

#--- MAIN ---#


print("\nTHE GEOLOGICAL ASSISTANT SYSTEM STARTS :")

files = open('allFileNames.txt','r')


while True:
    fileNames = []

    choice = input("\nChoose (1)Narrowing down the search space (2)Back to the previous search space (3)Showing the summary of a specific category, \n\
or press the ENTER key for quitting: ")

    if choice == '':
        break

    elif choice == '1':
        isFound = False

        # Ask the user some follow up questions:
        narrowing_choice = int(input(
            "\nChoose a category (1)fault sealing capacity (2)reservoir type (3)trap-formation (4)migration-time (5)pathway until a possible reservoir (6)accumulation: "))

        if filtering_category[narrowing_choice] == "fault sealing capacity":
            faultId = int(input("\nWhich fault do you want to choose? (0, or 1, or 2, or 3): "))
            fillingType = int(input("\nChoose the fault filling type (1)sealing (2)non-sealing: "))
            applied_filters.append("Fault " + str(faultId) + " is " + filling_category[fillingType] + ".")

        elif filtering_category[narrowing_choice] == "reservoir type":
            geoId = int(input("\nWhich possible reservoir do you want to choose? (5, or 8, or 11, or 14): "))
            sbf = int(input(
                "\nWhich submarine fan enviornment that the chosen GeoUnit locates? \n\
                Choose (1)feeder Channel (2)distributary channel (3)inter-channel (4)lobe (5)lobe fringe (6)basin plain: "))
            applied_filters.append("GeoUnit " + str(geoId) + " is in " + submarineFan_category[sbf] + ".")

        elif filtering_category[narrowing_choice] == "trap-formation":
            geoId = int(input("\nWhich possible reservoir can be trapped? Choose (5, or 8, or 11, or 14): "))
            applied_filters.append("GeoUnit " + str(geoId) + " can be trapped.")

        elif filtering_category[narrowing_choice] == "migration-time":
            faultId = int(input("\nFault 0, 1 and 2 were formed during the same period of time. Fault 3 is the youngest Fault. \n\
Which fault do you want to compare the migration time to? Choose (0 or 1 or 2 or 3): "))
            time_comparison = int(input(
                "\nChoose (1)migration happened AFTER the chosen fault was ceased (2)migration happened BEFORE the chosen fault was ceased: "))
            if time_comparison == 1:
                applied_filters.append("Migration happened AFTER the chosen fault " + str(faultId) + " was ceased.")
            elif time_comparison == 2:
                applied_filters.append("Migration happened BEFORE the chosen fault " + str(faultId) + " was ceased.")

        elif filtering_category[narrowing_choice] == "pathway until a possible reservoir":
            geoId = int(input("\nThere exists pathways from the source rock to the possible reservoir (5, or 8, or 11, or 14): "))
            applied_filters.append("There exists pathways from the source rock to the possible reservoir " + str(geoId) + ".")

        elif filtering_category[narrowing_choice] == "accumulation":
            geoId = int(input("\nWhich possible reservoir do you want to choose?  (5, or 8, or 11, or 14): "))
            ac = int(input("\nThe possible reservoir has accumulation or not (1)yes (2)no: "))
            if ac == 1:
                applied_filters.append("GeoUnit " + str(geoId) + " has accumulation.")
            elif ac == 2:
                applied_filters.append("GeoUnit " + str(geoId) + " does not have accumulation.")

        # The code for narrowing the search space:
        for file in files:
            readFile = open(file.strip('\n'), 'r')
            for ln in readFile:
                if filtering_category[narrowing_choice] == "fault sealing capacity":
                    if re.search("< " + str(faultId) + ".*Fault.*Filling: " + filling_category[fillingType] + ".*", ln):
                        isFound = True
                        break

                elif filtering_category[narrowing_choice] == "reservoir type":
                    if re.search("< " + str(geoId) + ".*GeoUnit.*SubmarineFan: " + submarineFan_category[sbf] + ",.*", ln):
                        isFound = True
                        break

                elif filtering_category[narrowing_choice] == "trap-formation":
                    if re.search("trapformation\(" + str(geoId) + ".*timeOf.*\)", ln):
                        isFound = True
                        break

                elif filtering_category[narrowing_choice] == "migration-time":
                    if age_comparison[time_comparison] == "younger":
                        if re.search("younger\(timeOf\(Migration\),timeOf\(Fault," + str(faultId) + "\)\)", ln):
                            isFound = True
                            break
                    elif age_comparison[time_comparison] == "older":
                        if re.search("younger\(timeOf\(Fault," + str(faultId) + "\),timeOf\(Migration\)\)", ln):
                            isFound = True
                            break

                elif filtering_category[narrowing_choice] == "pathway until a possible reservoir":
                    if re.search("< Pathway.*>", ln):
                        mp = interpreteMigrationPathway(ln)
                        if hasPathway(geoId, mp):
                            isFound = True
                            break

                elif filtering_category[narrowing_choice] == "accumulation":
                    if re.search("accumulation\(" + str(geoId) + ", " + accumulation_category[ac] + "\)", ln):
                        isFound = True
                        break

            if isFound:
                fileNames.append(file)
                isFound = False

            readFile.close()

        files.close()

        if not fileNames == []:
            fileNames_history.append(fileNames)
            # Update allFileNames.txt
            open('allFileNames.txt', 'w').close()
            files = open('allFileNames.txt', 'w')
            for file in fileNames:
                files.write(file)
            files.close()

            # intermediate report
            if filtering_category[narrowing_choice] == "fault sealing capacity":
                # print the result only until the chosen Fault
                printUpToGeoUnit(str(fault_to_geo[faultId]))
            elif filtering_category[narrowing_choice] == "reservoir type":
                # print the result only until GeoUnit
                printUpToGeoUnit(str(geoId))
            elif filtering_category[narrowing_choice] == "trap-formation":
                # print the result only until GeoUnit
                printUpToGeoUnit(str(geoId))
            elif filtering_category[narrowing_choice] == "migration-time":
                # print the result only until the chosen Fault
                printUpToGeoUnit(str(fault_to_geo[faultId]))
            elif filtering_category[narrowing_choice] == "pathway until a possible reservoir":
                # print the result only until GeoUnit
                printUpToGeoUnit(str(geoId))
            elif filtering_category[narrowing_choice] == "accumulation":
                # print the result only until GeoUnit
                printUpToGeoUnit(str(geoId))

        else:
            # allFileNames.txt remains the same
            print("\n!!!!NO CASES ARE MATCHED!!!!")
            del applied_filters[-1]


        files = open('allFileNames.txt', 'r')

    elif choice == '2':
        if len(applied_filters) > 0:
            applied_filters = applied_filters[:-1]
            del fileNames_history[-1]
            open('allFileNames.txt', 'w').close()
            files = open('allFileNames.txt', 'w')
            for file in fileNames_history[-1]:
                files.write(file)
            files.close()
            files = open('allFileNames.txt', 'r')
        else:
            print('It is not possible to backtrack more.')


    elif choice == '3':
        category_choice = int(input(
            "\nChoose a category for printing the results: \n\
(1)the type of hydrocarbon\n\
(2)all-fault (3)upto-fault (4)specific-fault \n\
(5)all-geounit (6)upto-geounit (7)specific-geounit \n\
(8)all-trapformation (9)upto-trapformation (10)specific-trapformation  \n\
(11)all-accumulation (12)upto-accumulation (13)specific-accumulation \n\
(14)all-pathway (15)all pathways not beyond a GeoUnit (16)pathway of a specific GeoUnit \n\
(17)migration-time \n\
(18)complete geological processes of hydrocarbon migration and accumulation \n\
(19)geological processes of hydrocarbon migration and accumulation not beyond a GeoUnit \n\
(20)all the scenarios (21)scenarios up to a specific GeoUnit: "))

        if print_result_category[category_choice] == "the type of hydrocarbon":
            printHydrocarbon()

        elif print_result_category[category_choice] == "all-fault":
            printAllFaults()

        elif print_result_category[category_choice] == "upto-fault":
            faultId = input("\nWhich fault do you want to check: Choose (0,or 1, or 2, or 3): ")
            printUptoFault(faultId)

        elif print_result_category[category_choice] == "specific-fault":
            faultId = input("\nWhich fault do you want to check: Choose (0,or 1, or 2, or 3): ")
            printSpecificFault(faultId)

        elif print_result_category[category_choice] == "all-geounit":
            printAllGeoUnits()

        elif print_result_category[category_choice] == "upto-geounit":
            geoId = input("\nWhich GeoUnit do you want to check? Choose (5, or 8, or 11, or 14): ")
            printUptoGeoUnit(geoId)

        elif print_result_category[category_choice] == "specific-geounit":
            geoId = input("\nWhich GeoUnit do you want to check? Choose (5, or 8, or 11, or 14): ")
            printSpecificGeoUnit(geoId)

        elif print_result_category[category_choice] == "all-trapformation":
            printAllTraps()

        elif print_result_category[category_choice] == "upto-trapformation":
            geoId = input("\nWhich GeoUnit do you want to check its trap? Choose (5, or 8, or 11, or 14): ")
            printUptoTrap(geoId)

        elif print_result_category[category_choice] == "specific-trapformation":
            geoId = input("\nWhich GeoUnit do you want to check its trap? Choose (5, or 8, or 11, or 14): ")
            printSpecificTrap(geoId)

        elif print_result_category[category_choice] == "all-accumulation":
            printAllAccumulations()

        elif print_result_category[category_choice] == "upto-accumulation":
            geoId = input("\nWhich GeoUnit do you want to check for its hydrocarbon accumulation? Choose (5, or 8, or 11, or 14): ")
            printUptoAccumulation(geoId)

        elif print_result_category[category_choice] == "specific-accumulation":
            geoId = input("\nWhich GeoUnit do you want to check for its hydrocarbon accumulation? Choose (5, or 8, or 11, or 14): ")
            printSpecificAccumulation(geoId)

        elif print_result_category[category_choice] == "all-pathway":
            printAllPathways()

        elif print_result_category[category_choice] == "all pathways not beyond a GeoUnit":
            geoId = input("\nWhich GeoUnit do you want to check? Choose (5, or 8, or 11, or 14): ")
            printAllPathwaysNotBeyondGeoUnit(geoId)

        elif print_result_category[category_choice] == "pathways of a specific GeoUnit":
            geoId = input("\nWhich GeoUnit do you want to check for its pathway? Choose (5, or 8, or 11, or 14): ")
            printSpecificPathways(geoId)

        elif print_result_category[category_choice] == "migration-time":
            printAllMigrationTimes()

        elif print_result_category[category_choice] == "complete geological processes of hydrocarbon migration and accumulation":
            printAllMigrationAccumulationHistory()

        elif print_result_category[category_choice] == "geological processes of hydrocarbon migration and accumulation not beyond a GeoUnit":
            geoId = input("\nWhich GeoUnit do you want to check? Choose (5, or 8, or 11, or 14): ")
            printSpecificMigrationAccumulationHistory(geoId)

        elif print_result_category[category_choice] == "all the scenarios":
            printWholeConfig()

        elif print_result_category[category_choice] == "scenarios up to a specific GeoUnit":
            geoId = input("\nWhich GeoUnit do you want to check? Choose (5, or 8, or 11, or 14): ")
            printUpToGeoUnit(geoId)

    # Bookkeeping for what we have filtered up to now:

    print("\n############## What you have chosen to keep in the search space: ##############")
    counter = 1
    for ln in applied_filters:
        print("(" + str(counter) + ") " + ln)
        counter += 1
    print("###############################################################################")
